import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Hope from '../views/Hope.vue'
import Telangana from '../views/Telangana.vue'
import India from '../views/India.vue'
import VueSweetalert2 from 'vue-sweetalert2';

Vue.use(VueRouter);
Vue.use(VueSweetalert2);

const routes = [
  {
    path: '/',
    name: 'Hope',
    component: Hope
  },
  {
    path: '/india',
    name: 'India',
    component: India
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/telangana',
    name: 'Telangana',
    component: Telangana
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
